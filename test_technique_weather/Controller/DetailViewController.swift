//
//  DetailViewController.swift
//  test_technique_weather
//
//  Created by hamza on 1/17/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var date_: String!
    var detail: [WeatherInfo]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        date.text = date_
        
        implementCell()
        tableDelegate()
    }
    
    func implementCell() {
        let nib = UINib(nibName: "DetailCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "detail_cell")
    }
    
    func tableDelegate() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension DetailViewController: UITableViewDelegate{}

extension DetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail_cell", for: indexPath) as! DetailTableViewCell
        cell.temperature.text = "\(detail[indexPath.row].getTemperatur())"
        cell.time.text = "\(detail[indexPath.row].getDate())h"
        cell.humidity.text = "\(detail[indexPath.row].getHumidity())"
        cell.pressure.text = "\(detail[indexPath.row].getPressure())"
        cell.wind_speed.text = "\(detail[indexPath.row].getWindSpeed())"
        return cell
    }
    
}
