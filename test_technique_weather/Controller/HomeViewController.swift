//
//  ViewController.swift
//  test_technique_weather
//
//  Created by hamza on 1/15/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    var weathers : [String : [WeatherInfo]]!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        getWeathers()
        implementCell()
    }
    
    func implementCell() {
        let nib = UINib(nibName: "WeatherCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "weather_cell")
    }

    func initTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }

    func getWeathers() {
        
         WeatherAPI.shared.loadWeathers(callback: { (response) in
            if let result = response as? [WeatherInfo] {
                let weatherViewModel = WeatherViewModel(weathers: result)
                self.weathers = weatherViewModel.getWeatherCleanList()
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail_segue" {
            let destination = segue.destination as! DetailViewController
            destination.detail = weathers[Array(weathers.keys)[sender as! Int]]
            destination.date_ = Array(weathers.keys)[sender as! Int]
        }
    }
    
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "detail_segue", sender: indexPath.row)
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard weathers != nil else {
            return 0
        }
        return weathers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weather_cell", for: indexPath) as! WeatherTableViewCell
        let weather = weathers[Array(weathers.keys)[indexPath.row]]
        cell.day.text = Array(weathers.keys)[indexPath.row]
        cell.temp.text = "\(weather!.first!.getTemperatur())°C"
        
        return cell
    }
    
}
