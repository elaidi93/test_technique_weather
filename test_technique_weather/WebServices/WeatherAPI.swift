//
//  WeatherAPI.swift
//  test_technique_weather
//
//  Created by hamza on 1/15/20.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation

class WeatherAPI {
    
    public static let shared = WeatherAPI()
    
    func loadWeathers(callback: @escaping (Any)->()) {
        let murl = Constant.api_url
        requestGet(url: murl,callback: callback)
    }
    
    func requestGet(url: String, callback: @escaping (Any)->()) {

        let url = URL(string: url)
        let cashPolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let request = NSMutableURLRequest(url: url! as URL, cachePolicy: cashPolicy, timeoutInterval: 10.0)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)

                callback("Pas de réponse, veuillez réessayer!" as String)

                return
            }
            guard let data = data else {
                callback("Request denied" as String)
                return
            }
            do {
                let parsed = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                callback(WeatherJsonParse.shared.parse(prevesion: parsed as! NSDictionary))
            } catch {
                print("json error: \(error)")
                callback("Request denied")
            }
        }
        task.resume()
    }
}
