//
//  WeatherViewModel.swift
//  test_technique_weather
//
//  Created by hamza on 1/16/20.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation
class WeatherViewModel {
    
    private var weatherList = [String: [WeatherInfo]]()
    private let dateFormatter = DateFormatter()
    private let timeFormatter = DateFormatter()
    private let dateTimeFormatter = DateFormatter()
    
    init(weathers: [WeatherInfo]) {
        
        dateTimeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "yyyy-MM-dd"
        timeFormatter.dateFormat = "HH"
        
        getCleanWeatherList(weathers: weathers)
        
    }
    
    func getCleanWeatherList(weathers: [WeatherInfo]) {
        for weather in weathers {
            
            if weatherList.keys.contains(dateFormatter.string(from: dateTimeFormatter.date(from: weather.getDate())!)) {
                
                let key = dateFormatter.string(from: dateTimeFormatter.date(from: weather.getDate())!)
                
                var list = weatherList[key]!
                
                list.append(WeatherInfo(date: timeFormatter.string(from: dateTimeFormatter.date(from: weather.getDate())!), temperature: Double(weather.getTemperatur()) - 273.15, humidity: weather.getHumidity(), windSpeed: weather.getWindSpeed(), pressure: weather.getPressure()))
                
                weatherList[key] = list
                
            } else {
                weatherList[dateFormatter.string(from: dateTimeFormatter.date(from: weather.getDate())!)] = [WeatherInfo(date: timeFormatter.string(from: dateTimeFormatter.date(from: weather.getDate())!), temperature: Double(weather.getTemperatur()) - 273.15, humidity: weather.getHumidity(), windSpeed: weather.getWindSpeed(), pressure: weather.getPressure())]
            }
        }
    }
    
    func getWeatherCleanList() -> [String: [WeatherInfo]] {return weatherList}
    
}
