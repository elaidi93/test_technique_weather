//
//  WeatherJsonParse.swift
//  test_technique_weather
//
//  Created by hamza on 1/16/20.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation

class WeatherJsonParse {
    
    static let shared = WeatherJsonParse()
    
    func parse(prevesion json: NSDictionary) -> [WeatherInfo] {
        
        var weatherDetails = [WeatherInfo]()
        
        let weathers = json["list"] as! [NSDictionary]
        
        for detail in weathers {
            let temp_detail = detail["main"] as! NSDictionary
            let wind = detail["wind"] as! NSDictionary
            
            let date = detail["dt_txt"] as! String
            let temperature = temp_detail["temp"] as! Double
            let humidity = temp_detail["humidity"] as! Double
            let wind_speed = wind["speed"] as! Double
            let pressur = temp_detail["pressure"] as! Double
            
            weatherDetails.append(WeatherInfo(date: date, temperature: temperature, humidity: humidity, windSpeed: wind_speed, pressure: pressur))
            
        }
        
        return weatherDetails
        
    }
    
}
