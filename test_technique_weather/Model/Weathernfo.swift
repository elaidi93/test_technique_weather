//
//  Weathernfo.swift
//  test_technique_weather
//
//  Created by hamza on 1/16/20.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation

class WeatherInfo {
    
    private var date: String!
    private var temperatur: Int!
    private var humidity: Double!
    private var windSpeed: Double!
    private var pressure: Double!
    
    init(date: String, temperature: Double, humidity: Double, windSpeed: Double, pressure: Double) {
        self.date = date
        self.temperatur = Int(temperature)
        self.humidity = humidity
        self.windSpeed = windSpeed
        self.pressure = pressure
    }
    
    func getDate() -> String { return date }
    func getTemperatur() -> Int { return temperatur}
    func getHumidity() -> Double { return humidity}
    func getWindSpeed() -> Double { return windSpeed }
    func getPressure() -> Double { return pressure }
    
}
